import cors from 'cors';
import express from 'express';
import { ApolloServer } from 'apollo-server-express';
//import typeDefs from "./models/OldFiles/schema.js"
//import resolvers from "./models/OldFiles/resolvers.js"
//import {gql} from "apollo-server"
import dbconnect from '../dbconnect'
//import author from "./modules/authorCategory/authorModel"
import mongoose from 'mongoose';
import jwt from 'jsonwebtoken';
import modules from './modules';
require('dotenv').config()

const verifyToken = token => new Promise((resolve,reject)=>{
	jwt.verify(token, process.env.ACCESS_TOKEN_SECRET,(err, decoded)=> {
    console.log("Decode", decoded)
	  if(err) resolve(null)
	  resolve(decoded)
	})	

})
dbconnect();
const app = express();



app.use(cors());
const server = new ApolloServer({
    typeDefs: modules.schema,
    resolvers: modules.resolvers,
    context: async({ req }) => {

      const token = req.headers.authorization || '';
    
      let me = await verifyToken(token)
      return {me}

    },
});

app.get('/', (req, res) => {
  res.send('<h3>Hello Ananthu! I just fixed my original project. I changed the requires to imports and filepaths to .js :D </h3>')
})
app.post('/', (req,res)=>{
const username = req.body.username
const user = {name: username}

const accessToken = jwt.sign(user,process.env.ACCESS_TOKEN_SECRET)
res.json({accessToken: accessToken})
})
// const auth = new author({
// _id: new mongoose.Types.ObjectId(),
// firstName: "Jane",
// lastName: "Goodall",

// })
//auth.save();
server.applyMiddleware({ app, path: '/graphql' });
const port = process.env.PORT || 8081;
app.listen({ port }, () => {
    console.log(`Apollo Server on http://localhost:${port}/graphql`);
});



 /* TODO: 
----------------------------------------------------------------------------------------------------------------------------------------------------
Monday - 6/28/21
        // Learn more on how to use group by (books with thier authors) (author and genere)
        // Learn More Apollo server schema "Defining a Schema"



        1st Aggregate:

db.getCollection('authors').aggregate([
 {$lookup: {
    from: 'Books',
    localField: '_id',
    foreignField: 'authID',
    as: 'Books'
 }},
 {$unwind: '$Books'},
    {
     $group : { _id:{_id: "$_id", genere: "$Books.genere"}, book:{$push: {
         title : "$Books.title", 
         author : "$Books.author",
         pages : "$Books.pages",
         cover : "$Books.cover", 
         genere : "$Books.genere", 
         createdBy : "$Books.createdBy" 
         
         }
         }}
   }
  ]),

   2nd Aggregate: 
   
db.getCollection('authors').aggregate([
 {$lookup: {
    from: 'Books',
    localField: '_id',
    foreignField: 'authID',
    as: 'Books'
 }},
 {$unwind: '$Books'},
    {
     $group : { _id:{_id: "$_id", author: "$Books.author"}, book:{$push: {
         title : "$Books.title", 
         author : "$Books.author",
         pages : "$Books.pages",
         cover : "$Books.cover", 
         genere : "$Books.genere", 
         createdBy : "$Books.createdBy" 
         
         }
         }}
   }
   ])


        */