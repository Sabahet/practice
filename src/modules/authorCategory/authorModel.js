import mongoose from 'mongoose';
const { Schema } = mongoose;

// Defining schema in mogoose

const authors = new Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
        required: true,
    },
    name: {
        type: String,
    }
});
export default mongoose.model('authors', authors, 'authors');