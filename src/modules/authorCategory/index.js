import AuthorModel from "./authorModel";
import AuthorSchema from './authorSchema';
import AuthorResolver from './authorResolver';

export default{
    AuthorModel,
    AuthorResolver,
    AuthorSchema,
};