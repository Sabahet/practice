import {gql} from 'apollo-server-express'

export default gql`
type Author{
    
    _id: ID!
    name: String
}
extend type Query{
    authors: [Author]
    
}

`