import authors from './authorModel.js'
import {combineResolvers,skip} from 'graphql-resolvers'
import { isAuthenticated } from '../authenticate.js'
const resolvers = {
    Query:{
        authors: combineResolvers (isAuthenticated,async(parent,args,context,info) =>{
            let finds = await authors.find({})
            return finds
    }),

}
}

export default resolvers;


