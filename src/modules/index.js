import userCategory from './userCategory'
import bookCategory from './bookCategory'
import authorCategory from './authorCategory'
import {gql} from 'apollo-server-express'
import jwt from 'jsonwebtoken'
require('dotenv').config()

const verifyToken = token => new Promise((resolve,reject)=>{
	jwt.verify(token, process.env.ACCESS_TOKEN_SECRET,(err, decoded)=> {
	  if(err) reject(err)
	  resolve(decoded)
	})	
})

const linkSchema = gql`
 type Query{
    _:Boolean
 }
 type Mutation {
    _:Boolean
 }
 type Subscription{
    _:Boolean
 }
`
const models = {
    userCategory: userCategory.UserModel,
    authorCategory: authorCategory.AuthorModel,
    bookCategory: bookCategory.BookModel,
    }
    
    export default{
        models,
        schema:[
            linkSchema,
            userCategory.UserSchema,
            authorCategory.AuthorSchema,
            bookCategory.BookSchema
        ],
        resolvers:[
            userCategory.UserResolver,
            authorCategory.AuthorResolver,
            bookCategory.BookResolver,
        ],
        verifyToken
    }