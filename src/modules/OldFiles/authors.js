
import mongoose from 'mongoose';
const { Schema } = mongoose;

// Defining schema in mogoose

const authors = new Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
        required: true,
    },
    ID: {
        type: Number,
        required: true,
    },
    firstName: {
        type: String,
    },
    lastName: {
        type: String,
    },
});
export default mongoose.model('authors', authors, 'authors');