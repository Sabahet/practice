import Book from  "./book.js"
import authors from "./authors.js"
import User from "./User.js"
import { UserInputError } from "apollo-server-express";
//import { Mongoose } from "mongoose";
//import book from "../models/book.js";
import jwt from 'jsonwebtoken'
import bcrypt from 'bcrypt'


const tokenTime= 3*24*60*60;

const createToken = (id) =>{
  return jwt.sign({id},"This is my not so secret secret", {
    expiresIn: tokenTime
  })

}
const resolvers  = {
    Query:{
        oneAuth: (parent, args, context, info) =>{
            //return Book.find(Book => Book.authID === args.authID);
            //console.log("This is args", args)
           return Book.findOne({authID: args.authID})
        },
        
        books: async() =>{
            let finder = await Book.find()
            //console.log(finder)
            return finder
        },
        authors: async() =>{
            let finds = await authors.find({})
            //console.log(finds)
            return finds
        },
        fictionBooks: async() =>{
            let finder = await Book.find({genere: 'Fiction'})
            return finder
            //return Book.filter((element => element.genere == "Fiction"))

            // books.forEach((element) => {
            //     if(element.genere == "Fiction" ){
            //         fic.push(element)
            //     }
            // })

        },

        booksAndThierAuthor: async() =>{
            //book.find -> LOOP -> For each -> books.findOne

        return Book.aggregate([
                {
                   $lookup:
                     {
                       from: "authors",
                       localField: "authID",
                       foreignField: "ID",
                       as: "author"
                    
                     }
                },
                { $unwind: "$author" }
                
                ])



        //     let modifiedBooksAddedAuther = Book.map((book) => {
		// 	let foundTheAutherElement = authors.find((author)=> book.authID === author.ID)
		// 	book.author=foundTheAutherElement
		// 	return book;
        //    })
        //    return modifiedBooksAddedAuther
        },



        // booksAndThierAuthor: () =>{
        //     let bookAndAuth = [];


        //     books.forEach((book,index1) => {
        //       authors.find((author,index2) =>{
        //             if( book.authID == author.ID){
        //                 books[index1].author = authors[index2]
        //                 bookAndAuth.push(author)
        //             }
        //     } )
        // })
          
        //     return bookAndAuth
        // }
    },

    Mutation: {
    addBook: (_, {info:{title,author,cover,pages,authID,genere}}) => {
        const newBook = {
        title,
        author,
        cover,
        pages,
        authID,
        genere,

        }
        //console.log("This is new book", newBook)
        Book.create(newBook)
return newBook;

    },
    deleteBook: async (_, ID) => {

      let deleted = await Book.deleteOne({_id: ID._id}, function (err) {
           if(err){ 
               console.log("Theres an error")
       }else{
           console.log("Book has been deleted")
       }
       })
       return deleted
    },
    
   editBook: async (_,{_id,input:{title,author,cover,pages,authID,genere}}) => {
       console.log(_id)

       let foundBook =  await Book.findById(_id);
       foundBook.title = title
       foundBook.author = author
       foundBook.cover = cover
       foundBook.pages = pages
       foundBook.authID = authID
       foundBook.genere = genere
      
       return foundBook.save()

    },
    signUp: async (_,{username,email,password}) =>{
        console.log(username)
        
       let exists = await User.find({
           $or: [
               {username: username},
               {email: email}
            ]
        },function(err,docs){
        if(err){
            throw Error("There was an error")
        }else{
            console.log("passed")
        }

        });

        console.log(exists.username  )
        //console.log(t)
       if(exists.password === password || exists.email === email){
           throw Error("This username and/or email is in use")
       }

      const newUser = {
            username,
            email,
            password,
    
        }

       let newUserCreated = await User.create(newUser)

        // let token = createToken(newUser)
        // console.log(token)

        return { username: newUserCreated.username, _id: newUserCreated._id }
    },

//Add users id and create token 
    login: async (_, {username,password}) =>{
         let token = createToken(newUser)
   
        let authenticate = await User.findOne({username: username})
        if(authenticate){
        const auth = await bcrypt.compare(password,authenticate.password)
        if(auth){
            return {username} // returns an object, that has two keys, one is token and other is user object+ user details(_id and Username)
        }
        throw Error("Username and/or Password incorrect")
        }
        throw Error("Username and/or Password incorrect")
    },

    active: (_, {toggleActivate})=>{
        toggleActivate = !toggleActivate;
        User.save(toggleActivate)
        return User
    }
}
};

//module.exports = resolvers;

//Udpate Delete to asynch await and return an object for status

// TODO: 
// Add users id and create token check above

export default resolvers;