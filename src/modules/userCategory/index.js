import UserModel from "./userModel";
import UserSchema from './userSchema';
import UserResolver from './userResolver';

export default{
    UserModel,
    UserResolver,
    UserSchema,
};