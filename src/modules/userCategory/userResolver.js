import User from './userModel'
import bcrypt from 'bcrypt'
import jwt from 'jsonwebtoken'

const createToken = (data) =>{
    return jwt.sign(data,process.env.ACCESS_TOKEN_SECRET, {
      expiresIn: 10000
    })
}
const resolvers = {
    
    Mutation:{
        signUp: async (_,{username,email,password,role}) =>{
            console.log(username)
            
           let exists = await User.exists({
               $or: [
                   {username: username},
                   {email: email}
                ]
            })
            console.log(exists)
           if(exists){
               throw Error("This username and/or email is in use")
           }
          const newUser = {
                username,
                email,
                password,
                role
            }
    
           let newUserCreated = await User.create(newUser)
    
            // let token = createToken(newUser)
            // console.log(token)
    
            return { username: newUserCreated.username, _id: newUserCreated._id }
        },
        login: async (_, {username,password}) =>{
            
            
            let foundUser = await User.findOne({username: username}, {_id: true, username: true, email: true, password: true })
            let token = createToken({_id: foundUser._id})

            if(foundUser){
            const auth = await bcrypt.compare(password,foundUser.password)
            delete foundUser.password
            if(auth){
                return {user: foundUser, token} // returns an object, that has two keys, one is token and other is user object+ user details(_id and Username)
            }
            throw Error("Username and/or Password incorrect")
            }
            throw Error("Username and/or Password incorrect")
        },
        toggleActivate: (_, {toggleActivate})=>{
            toggleActivate = !toggleActivate;
            User.save(toggleActivate)
            return User
        }
    }
}

export default resolvers;