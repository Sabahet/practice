import mongoose from 'mongoose';
const { Schema } = mongoose;
import bcrypt from 'bcrypt'
// Defining schema in mogoose

const User = new Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
        required: true,
    },
    username: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    }, 
    active:{
        type: Boolean,
        default: true,
        required: true,
    } ,
    role: {
        type:String,
    }

});

User.pre('save',async function(next) {
    const salt = await bcrypt.genSalt();
    this.password = await bcrypt.hash(this.password, salt);
    next();
})
export default mongoose.model('User', User, 'User');