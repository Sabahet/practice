import {gql} from 'apollo-server-express'

export default gql`
type User{
    _id:ID!
    username: String!
    email: String!
    password: String!
    token: String
    role: String!
}
type SignUpReturn{
    username:String,
    _id: ID!

}
type lessUser{
    _id: ID!
    username: String!
    email: String!
}
type loginData{
    token: String!
    user: lessUser
}
extend type Mutation{
        signUp(username:String,email:String, password:String!, role: String ): SignUpReturn
        login(username:String!, password: String!): loginData
        toggleActivate(active:Boolean):User
}

`