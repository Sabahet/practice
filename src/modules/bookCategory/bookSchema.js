import {gql} from 'apollo-server-express'
import Author from '../authorCategory/authorSchema.js'

export default gql`
type Book{
    _id: ID!
    title: String
    author: String
    cover: String
    pages: Int
    authID: ID!
    genere: String
    createdBy: ID!
}
input BookData {
    title: String
    author: String
    cover: String
    pages: Int
    authID: ID!
    genere: String
    createdBy: ID!
}
type bookWithAuthor{
title: String
cover: String
pages: Int
author: Author
}
input InputData {
    title: String
    author: String
    cover: String
    pages: Int
    authID: ID!
    genere: String
    createdBy: ID!
}
extend type Query{
    books: [Book]
    fictionBooks: [Book]
    booksAndThierAuthor: [bookWithAuthor]
    oneAuth(authID: Int!) : Book
    booksCreatedBy: [Book]
}
extend type Mutation{
    addBook(info: BookData ) : Book
        deleteBook(_id: ID!) : Book
        editBook(_id:ID!, input: InputData): Book
}


`