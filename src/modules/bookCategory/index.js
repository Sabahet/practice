import BookModel from "./bookModel";
import BookSchema from './bookSchema';
import BookResolver from './bookResolver';

export default{
    BookModel,
    BookResolver,
    BookSchema,
};