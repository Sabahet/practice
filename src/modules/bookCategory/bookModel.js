import mongoose from 'mongoose';
const { Schema } = mongoose;

// Defining schema in mogoose

const Books = new Schema({
    _id: {
        type: mongoose.Schema.Types.ObjectId,
        auto: true,
        required: true,
    },
    title: {
        type: String,
        required: true,
    },
    author: {
        type: String,
    },
    cover: {
        type: String,
    },
    pages: {
        type: Number,
    },
    genere:{
        type: String,
    },
    authID:{
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    },
    createdBy:{
        type: mongoose.Schema.Types.ObjectId,
        required: true,
    }
});
export default mongoose.model('Books', Books, 'Books');