import Book from './bookModel.js'
import Author from '../authorCategory/authorModel'
import User from "../userCategory/userModel"
import {combineResolvers,skip} from 'graphql-resolvers'
import { isAuthenticated } from '../authenticate.js'
const resolvers = {
    Query:{
        oneAuth: (parent, args, context, info) =>{
            //return Book.find(Book => Book.authID === args.authID);
            //console.log("This is args", args)
           return Book.findOne({authID: args.authID})
        }, 
        books: async() =>{
            let finder = await Book.find()
            //console.log(finder)
            return finder
        },
        fictionBooks: async() =>{
            let finder = await Book.find({genere: 'Fiction'})
            return finder
        },
        booksAndThierAuthor: async() =>{
            //book.find -> LOOP -> For each -> books.findOne

        return Book.aggregate([
                {
                   $lookup:
                     {
                       from: "authors",
                       localField: "authID",
                       foreignField: "ID",
                       as: "author"
                    
                     }
                },
                { $unwind: "$author" }
                
                ])
            },

        booksCreatedBy: combineResolvers(isAuthenticated, async(parent,args,context,info) =>{
                let theirBooks = await Book.find({createdBy: context.me._id})
                return(theirBooks)
            })



    },
    Mutation:{
        //link by id, Me.id
    //     combineResolvers (isAuthenticated,async(parent,args,context,info) =>{
    //         let finds = await authors.find({})
    //         return finds
    // })
        addBook: combineResolvers (isAuthenticated, async (_, {info:{title,author,cover,pages,genere}},context) => {
            let match = await Author.findOne({name: author},{_id: true, name:true})

            if(match == null){
                match = await Author.create({name: author})
            }
//upsert
            const newBook = {
            title,
            author,
            cover,
            pages,
            authID: match._id,
            genere,
            createdBy: context.me._id,
    
            }

        await Book.create(newBook)
    return newBook;
        }),

        deleteBook: async (_,_id) => {

            let deleted = await Book.deleteOne({_id: _id}, function (err) {
                 if(err){ 
                     console.log("Theres an error")
             }else{
                 console.log("Book has been deleted")
             }
             })
             return deleted
          },
          editBook:combineResolvers(isAuthenticated, async (_,{_id,input:{title,author,cover,pages,authID,genere}},context) => {
            let currentUser = await User.findById(context.me._id)
            let foundBook =  await Book.findById(_id);
            foundBook.title = title
            foundBook.author = author
            foundBook.cover = cover
            foundBook.pages = pages
            foundBook.authID = authID
            foundBook.genere = genere
           // console.log(currentUser.role)
            if(context.me._id == foundBook.createdBy || currentUser.role == 'Admin'){
                return foundBook.save()
            }else{
                throw Error ("You dont have permissions to edit someone elses book")
            }
     
         }),
    }

}

export default resolvers;