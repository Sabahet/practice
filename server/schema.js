import {gql} from 'apollo-server'

//This is what our Query will look like

const typeDefs = gql`
type Book{
    _id: ID!
    title: String
    author: String
    cover: String
    pages: Int
    authID: Int
    genere: String
}
type Author{
    
    ID: Int
    firstName: String
    lastName: String
}
type User{
    _id:ID!
    username: String!
    email: String!
    password: String!
    token: String
}
type SignUpReturn{
    username:String,
    _id: ID!

}
type bookWithAuthor{
title: String
cover: String
pages: Int
author: Author
}

type Query{
    books: [Book]
    authors: [Author]
    fictionBooks: [Book]
    booksAndThierAuthor: [bookWithAuthor]
    oneAuth(authID: Int!) : Book
    
}
input BookData {
    title: String
    author: String
    cover: String
    pages: Int
    authID: Int
    genere: String
}
input InputData {
    title: String
    author: String
    cover: String
    pages: Int
    authID: Int
    genere: String
}
type Mutation{
        addBook(info: BookData ) : Book,
        deleteBook(_id: ID!) : Book,
        editBook(_id:ID!, input: InputData): Book
        signUp(username:String,email:String password:String!): SignUpReturn
        login(username:String!, password: String!): User
        toggleActivate(active:Boolean):User
}

`;
export default typeDefs;

// Edit a book all feilds even ID --  Using input types.

// Go over JASON Web Tokens 
