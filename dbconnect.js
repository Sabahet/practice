
import mongoose from "mongoose"
export default async () => {
    try {
        // eslint-disable-next-line no-console
        console.log(`Trying to Connect DB: `);
        mongoose.Promise = global.Promise;
        await mongoose.connect(process.env.DATABASE_LINK, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
        });
    } catch (err) {
        // eslint-disable-next-line no-console
        console.log(`error DB Connection: ${err}`);
    }
};

// Create an Admin that can do anything
